﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
public GameObject logo;
private AudioSource click;
[SerializeField] Animator anim;
[SerializeField] Button[] buttons;
    
    void Start(){
        click = GetComponent<AudioSource>();
        Cursor.visible = true;
    }
    public void PulsaStart(){
            logo.SetActive(false);
            click.Play();
            StartCoroutine(Alpha());
            

    }
        public void PulsaCredits(){
            click.Play(); 
            SceneManager.LoadScene("CreditsScreen");
    }
    public void PulsaExit(){
        Application.Quit();
    }

    private IEnumerator Alpha(){
        bool salir = false;
        Color tmp;

        while(!salir){
            foreach (Button b in buttons){
                if(b.image.color.a > 0){
                    tmp = b.image.color;
                    tmp.a-=0.05f;
                    b.image.color = tmp;
                }else{
                    salir = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }
        anim.SetTrigger("OnClick");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene("GameplayScreen");
    }
}
