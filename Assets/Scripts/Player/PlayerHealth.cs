﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public float lifes = 40f;
    [SerializeField] GameObject[] canvasBlood;
    [SerializeField] AudioSource hearth;
    public float currentTime = 0;
    private float timeToHeal = 20;

    void Start(){
        foreach(GameObject c in canvasBlood){
            c.SetActive(false);
        }
    }
    void Update(){
        //Recuperar Vida
        currentTime += Time.deltaTime;
        if(currentTime >= timeToHeal){
            currentTime = 0;
            if(lifes < 40){
                lifes += 5;
            }
        }

        if(lifes >= 40){
            foreach(GameObject c in canvasBlood){
                c.SetActive(false);
            }
        }
        else if( lifes < 40 && lifes >= 30){
            canvasBlood[0].SetActive(true);
            canvasBlood[1].SetActive(false);
            canvasBlood[2].SetActive(false);
            canvasBlood[3].SetActive(false);
        }
        else if(lifes < 30 && lifes >= 20){
            canvasBlood[0].SetActive(false);
            canvasBlood[1].SetActive(true);
            canvasBlood[2].SetActive(false);
            canvasBlood[3].SetActive(false);
        }
        else if(lifes < 20 && lifes >= 10){
            canvasBlood[0].SetActive(false);
            canvasBlood[1].SetActive(false);
            canvasBlood[2].SetActive(true);
            canvasBlood[3].SetActive(false);
        }
        else if (lifes < 10 && lifes > 0){
            canvasBlood[0].SetActive(false);
            canvasBlood[1].SetActive(false);
            canvasBlood[2].SetActive(false);
            canvasBlood[3].SetActive(true);
        }
        else if(lifes <= 0){
            SceneManager.LoadScene("LoseScreen");
        }
    }
    public void lifess(float damage){
        lifes -= damage; 
    }

    public void ManagerLife(){
        if(lifes == 0){
            SceneManager.LoadScene("LoseScreen");
        }
    }
    
}
