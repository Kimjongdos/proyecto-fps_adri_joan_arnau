﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerMovement : MonoBehaviour {

    private CharacterController controller;
    private Vector2 axis;
    public float speed;
    public Vector3 moveDirection;
    private float forceToGround = Physics.gravity.y;

    public float jumpSpeed;
    private bool jump;
    public float gravityMagnitude = 1.0f;
     
     public int Sensibilidad;
     private float MouseY = 0;

     //La llave
     public bool key = false;
     private CapsuleCollider capCollider;


     public GameObject Arma1;
     public GameObject Arma2;

     public bool HaveTheKey = false;
    
   // public GameObject UIEscopeta;
    //public GameObject UIPistola;
     

    void Start ()
    {
        controller = GetComponent<CharacterController>();
        Arma1.SetActive(true);
        Arma2.SetActive(false);
        capCollider = GetComponent<CapsuleCollider>();
    }

    void Update ()
    {
       
        Movement();
        RotacionY();
        SelectionWeapon();

    }

    public void SetAxis(Vector2 inputAxis)
    {
        axis = inputAxis;
    }

    public void StartJump()
    {
        if(!controller.isGrounded) return;

        moveDirection.y = jumpSpeed;
        jump = true;
    }
    public void RotacionY(){
        MouseY += Input.GetAxis("Mouse X") * Sensibilidad;
        transform.localEulerAngles = new Vector3(0,MouseY,0);
    }
    public void Movement(){

         if(controller.isGrounded && !jump)
        {
            moveDirection.y = forceToGround;
        }
        else
        {
            jump = false;
            moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
        }

        Vector3 transformDirection = axis.x * transform.right + axis.y * transform.forward;

        moveDirection.x = transformDirection.x * speed;
        moveDirection.z = transformDirection.z * speed;

        controller.Move(moveDirection * Time.deltaTime);
    }

    public void SelectionWeapon(){
        if(Input.GetKeyDown(KeyCode.Alpha1)){
           //Escopeta.transform.localScale = new Vector3(1, 1, 1);
            Arma1.SetActive(true);
            Arma2.SetActive(false);
        }
        if(Input.GetKeyDown(KeyCode.Alpha2)){
            Arma1.SetActive(false);
            Arma2.SetActive(true);
        }

    }
}
