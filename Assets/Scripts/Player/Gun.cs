﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour {

    public AudioSource GunSound;
    public float maxDistance;
    public ParticleSystem ParticulaDestello;
    public ParticleSystem ParticulaDestello2;

 

    public int maxAmmo;
    public int currentAmmo;
    public Transform tdisparo;
    
    public float Distance;
  

    public Animator GunShoot;

    public bool shooting = true;

    public float TimeToReload = 3f;

    public GameObject PistolaToInstatiate;
    public GameObject Pistola;

    public GameObject Impacto;
    public Text MunicionPistola;

    //Luz Disparo
    [SerializeField] GameObject shootLight;

    private bool isScoped = false;
    //Recarga
    private bool recargando = false;

    //Ataque
    public float damage;

   
   

    private void Start()
    {
        currentAmmo = maxAmmo;
        shooting = true;
        shootLight.SetActive(false);
    }
    void Update(){

        Shoot();
        TextMunicion();
    }

    public void Shoot()
    {
        if(currentAmmo<=0 && Input.GetMouseButtonDown(0) && !recargando){
            StartCoroutine(Reloading());
        }
        else if(Input.GetMouseButtonDown(0)){
            
                if(shooting == true){
                    //shooting = true;
                    currentAmmo--;
                    
                    StartCoroutine(LuzdeDisparo());
                    GunSound.Play();
                    Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width /2,Screen.height/2,0));
                    RaycastHit hitInfo;
                    GunShoot.SetTrigger("shoot");
                    ParticulaDestello.Play();
                    ParticulaDestello2.Play();
                    
                     if(Physics.Raycast(ray, out hitInfo)){
                        if(hitInfo.collider.gameObject.tag == "Enemy"){
                            hitInfo.collider.gameObject.GetComponent<EnemyLife>().Damaged(damage);
                        }
                        else Instantiate(Impacto,hitInfo.point,Quaternion.LookRotation(hitInfo.normal));
                    }

                    
                }
                   
        }else{
  
        }
        //Recarga
        if(Input.GetKeyDown(KeyCode.R) && currentAmmo != maxAmmo && !recargando){
            StartCoroutine(Reloading());
        }

        if(Input.GetMouseButtonDown(1)){
            isScoped =! isScoped;
            GunShoot.SetBool("Apuntando",isScoped);
            
        }

        if(Input.GetMouseButtonDown(0) && isScoped){
            GunShoot.SetTrigger("ApuntandoDisparo");
        }
        
    }

    IEnumerator Reloading(){
        shooting = false;
        recargando = true;
        Instantiate(PistolaToInstatiate,transform.position,Quaternion.identity,null);
        Pistola.SetActive(false);
       
        yield return new WaitForSeconds(TimeToReload);
        Pistola.SetActive(true);
        currentAmmo = maxAmmo;
        shooting = true;
        recargando = false;
    }

    public void TextMunicion(){
        MunicionPistola.text = currentAmmo.ToString() + "/" + maxAmmo.ToString();
    }

    IEnumerator LuzdeDisparo(){
        shootLight.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        shootLight.SetActive(false);

    }

}

