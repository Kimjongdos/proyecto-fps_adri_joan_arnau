﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granade : MonoBehaviour
{
    public CapsuleCollider coll;
    public Rigidbody rbd;

    public ParticleSystem explosion;

    public int explosionRadius;

    public int explosionForce;
    public MeshRenderer mesh;

    public AudioSource granadesound;
    public float timeToExplode = 2f;

    public float damage = 15f;
        void OnCollisionEnter(Collision other)
        {
            if(other.gameObject.tag == "Floor"){
                StartCoroutine(BUM());  
            }
        }
    
    IEnumerator BUM(){
        yield return new WaitForSeconds(timeToExplode);
        Explosion();
    }
    public void Explosion()
    {
        Destroy(mesh);
        //Lanzar particula
        explosion.Play();
        granadesound.Play();

        Collider[] cols = Physics.OverlapSphere(this.transform.position, explosionRadius);
        foreach(Collider c in cols)
        {
            if(c.attachedRigidbody != null)
            {
                c.attachedRigidbody.AddExplosionForce(explosionForce, this.transform.position, explosionRadius, 1, ForceMode.Impulse);
                PlayerHealth player = c.GetComponent<PlayerHealth>();
                EnemyLife enemy = c.GetComponent<EnemyLife>();
                if(player != null){
                    Debug.Log("mama");
                    player.lifess(damage);
                }
                if(enemy != null){
                    enemy.Damaged(damage);
                }  
            }
             
        }
        
         Destroy(gameObject,2f);
    }
}
