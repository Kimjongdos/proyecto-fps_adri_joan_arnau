﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabGun : MonoBehaviour
{
   public Rigidbody rbd;
   private BoxCollider bCollider;
   private AudioSource sound;

    // Update is called once per frame
    void Start()
    {
        bCollider = GetComponent<BoxCollider>();
        rbd.AddForce(0,0,2f);
        sound = GetComponent<AudioSource>();
        StartCoroutine(DestroyInSeconds());
    }

    void OnCollisionEnter(Collision other){
        if(other.gameObject.tag == "Floor"){
            sound.Play();
            Debug.Log("suelo");
        }
    }

    IEnumerator DestroyInSeconds(){
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);
    }
}
