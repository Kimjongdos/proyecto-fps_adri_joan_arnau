﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Escopeta : MonoBehaviour {

    public AudioSource GunSound;
    public float maxDistance;
    public ParticleSystem ParticulaDestello;
    public ParticleSystem ParticulaDestello2;
    public int maxAmmo;
    public int currentAmmo;
    public Transform tdisparo;
    
    public float Distance;
    

    public Animator GunShoot;

    public bool shooting = true;

    public float TimeToReload = 3f;

    public GameObject PistolaToInstatiate;
    public GameObject Pistola;

    public GameObject Impacto;
    public Text MunicionEscopeta;
    
    public GameObject shootLight;
    private bool isScoped = false;

    private bool recargando = false;

    public float fireRate = 15;

    private float nextTimeTofire = 0f;

    public float damage;

    private void Start()
    {
           
        currentAmmo = maxAmmo;
        shooting = true;
        shootLight.SetActive(false);
        MunicionEscopeta.text = currentAmmo.ToString() + "/" + maxAmmo.ToString();

     
    }
    void Update(){

        Shoot();
        TextMunicion();
    }

    public void Shoot()
    {
        if(currentAmmo<=0 && Input.GetMouseButtonDown(0) && !recargando){
            
            StartCoroutine(Reloading());

        }else if(Input.GetMouseButtonDown(0) && Time.time >= nextTimeTofire){
            
                if(shooting == true){
                    nextTimeTofire = Time.time + 1f/fireRate;
                    currentAmmo--;
                   
                   StartCoroutine(LuzDisparo());
                    GunSound.Play();
                    Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width /2,Screen.height/2,0));
                    RaycastHit hitInfo;
                    GunShoot.SetTrigger("shoot");
                    ParticulaDestello.Play();
                    ParticulaDestello2.Play();
                    
                     if(Physics.Raycast(ray, out hitInfo)){
                         if(hitInfo.collider.gameObject.tag == "Enemy"){
                            hitInfo.collider.gameObject.GetComponent<EnemyLife>().Damaged(damage);
                        }
                        else Instantiate(Impacto,hitInfo.point,Quaternion.LookRotation(hitInfo.normal));
                    }
                    
                }
                   
        }
        if(Input.GetMouseButtonDown(1)){
            isScoped =! isScoped;
            GunShoot.SetBool("Apuntando",isScoped);
            
        }
        //Recarga
        if(Input.GetKeyDown(KeyCode.R) && currentAmmo != maxAmmo && !recargando){
            StartCoroutine(Reloading());
        }
        
    }

    IEnumerator Reloading(){
        shooting = false;
        recargando = true;
        Instantiate(PistolaToInstatiate,transform.position,Quaternion.identity,null);
        Pistola.SetActive(false);
       
        yield return new WaitForSeconds(TimeToReload);
        Pistola.SetActive(true);
        currentAmmo = maxAmmo;
        shooting = true;
        recargando = false;
    }
     public void TextMunicion(){
        MunicionEscopeta.text = currentAmmo.ToString() + "/" + maxAmmo.ToString();

    }
    IEnumerator LuzDisparo(){
        shootLight.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        shootLight.SetActive(false);
    }

}

