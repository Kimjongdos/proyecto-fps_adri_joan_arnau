﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GranadeManager : MonoBehaviour
{
    public GameObject Granada;
    public Transform Player;
    // Update is called once per frame
    public float throwforce = 40f;

     public int Granadas;
     public Text Granadastext;
    
    void Update(){
        Throw();
        Granadastext.text = Granadas.ToString();
    }

    void Start(){
        Granadas = 0;
    }

    public void Throw(){
        if(Input.GetKeyDown(KeyCode.G) && Granadas >0){
            Granadas--;
            GameObject gran = Instantiate(Granada,transform.position,transform.rotation);
            Rigidbody rb = gran.GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * throwforce, ForceMode.VelocityChange);

            
        }
        
    }
    
        
    
}
