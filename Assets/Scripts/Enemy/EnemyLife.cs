﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class EnemyLife : MonoBehaviour
{
    public float health;
    public ParticleSystem ps;
    public MeshRenderer mesh;
    public Enemy2 itsMe;
 
    void Dead(){
        if(health <= 0){
            Destroy(mesh);
            itsMe.enabled = false;
            ps.Play();
            Destroy(this.gameObject, 2f);
        }
    }
    public void Damaged(float damage){
        health -= damage;
        Dead();
    }
}
