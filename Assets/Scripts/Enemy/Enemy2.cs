﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[RequireComponent(typeof(AudioSource))]



public class Enemy2 : MonoBehaviour
{
    public enum State {Patrol, Chase}
    public State state;
    private NavMeshAgent agent;
    [Header("Enemy Properties")]
    public float enemyDamage;
    public Transform RaycasTransform;

    [Header("Patrol")]
    private int currentNode = 0;
    public Transform[] pathNodes;
    private bool nearNode = true;
    

    [Header("Target Detection")]
    public float radius;
    public float chaseRadius;
    public LayerMask targetMask;
    public bool targetDetected = false;
    private Transform targetTransform;

    public float TimeToShoot;
    private float time;

     [Header("Cosas que hace el enemigo para que se vea to chuli")]

     public ParticleSystem disparoPART;
     public AudioSource levitando;
     public AudioSource shoot;

     public GameObject shootLight;



    void Start(){
        agent = GetComponent<NavMeshAgent>();
        shootLight.SetActive(false);

    }
    void Update(){
        switch(state){
            case State.Patrol:
                Patrol();
                break;
            case State.Chase:
                Chase();
                break;
            default:
                break;
        }
        time += Time.deltaTime;
        levitando.Play();
    }
    private void FixedUpdate(){
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if(cols.Length != 0){
            targetDetected = true;
            targetTransform = cols[0].transform;
        }
    }

    void Patrol(){
        if(targetDetected){
            SetChase();
            return;
        }
        if(agent.remainingDistance <= 0.1f){
            if(nearNode) GoNearNode();
             else GoNextNode();
        } 
    }
    

    void Chase(){
        if(!targetDetected){
            nearNode = true;
            SetPatrol();
        }
        agent.SetDestination(targetTransform.position);
                
        RaycastHit hit;
        if(TimeToShoot<time){
          
            if(Physics.Raycast(RaycasTransform.position,transform.forward, out hit,10f)){
               


                if (hit.collider.gameObject.tag == "Player"){
                    
                    if(targetDetected){
                        disparoPART.Play();
                        shootLight.SetActive(true);
                        time = 0;
                        StartCoroutine(LuzDisparo());
                        shoot.Play();
                    }
                    
                    
                    hit.collider.gameObject.GetComponent<PlayerHealth>().lifess(enemyDamage);
                    
                }
            }
        }

    }

    void SetPatrol(){
        state = State.Patrol;
    }

    void SetChase(){
        agent.SetDestination(targetTransform.position);
        radius = chaseRadius;
        agent.stoppingDistance = 3f;

        state = State.Chase;
    }

     void GoNextNode()
    {
        currentNode++;
        if(currentNode >= pathNodes.Length) currentNode = 0;

        agent.SetDestination(pathNodes[currentNode].position);
    }

    void GoNearNode(){
        nearNode = false;
        float minDistance = Mathf.Infinity;
        for(int i = 0; i < pathNodes.Length; i++)
        {
            if(Vector3.Distance(transform.position, pathNodes[i].position) < minDistance)
            {
                minDistance = Vector3.Distance(transform.position, pathNodes[i].position);
                currentNode = i;
            }
        }
        agent.SetDestination(pathNodes[currentNode].position);
    }

    private void OnDrawGizmos(){
        Color color = Color.green;
        if(targetDetected) color = Color.red;
        color.a = 0.3f;
        Gizmos.color = color;
        Gizmos.DrawSphere(this.transform.position, radius);
    }
    IEnumerator LuzDisparo(){
        shootLight.SetActive(true);
        yield return new WaitForSeconds(0.2f);
        shootLight.SetActive(false);
    }
}
