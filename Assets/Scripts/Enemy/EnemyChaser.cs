﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyChaser : MonoBehaviour
{
    private NavMeshAgent agent;
    public float damage;

    [Header("Target Detection")]
    public float radius;
    public LayerMask targetMask;
    public bool targetDetected = false;
    private Transform targetTransform;

    void Start(){
         agent = GetComponent<NavMeshAgent>();
    }
    private void FixedUpdate(){
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if(cols.Length != 0){
            targetDetected = true;
            targetTransform = cols[0].transform;
        }
    }

    void Update(){
        agent.SetDestination(targetTransform.position);
    }

    void OnCollisionEnter(Collision col){
        if(col.gameObject.tag == "Player"){
            col.gameObject.GetComponent<PlayerHealth>().lifess(damage);
        }
    }
}
