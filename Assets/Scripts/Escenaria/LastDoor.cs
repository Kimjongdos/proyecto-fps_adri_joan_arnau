﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LastDoor : MonoBehaviour
{
    
    public BoxCollider coll;
    public Animator animDoor;
    public AudioSource DoorOpenSound;
   

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag ==  "Player"){
            animDoor.SetTrigger("open");
            DoorOpenSound.Play();
            StartCoroutine(Uwin());
        }
        IEnumerator Uwin(){
            yield return new WaitForSeconds(1f);
            SceneManager.LoadScene("WinScene");
        }
    }
}