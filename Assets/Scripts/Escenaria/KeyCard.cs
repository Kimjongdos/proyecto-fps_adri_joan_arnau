﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCard : MonoBehaviour
{
   public CapsuleCollider coll;

   public PlayerMovement player;

   public GameObject canvasKey;

   public AudioSource CatchKey;

   

    // Update is called once per frame
  void Start(){
        canvasKey.SetActive(false);
    }

    private void OnTriggerStay(Collider other) {
        canvasKey.SetActive(true);

        if(Input.GetKeyDown(KeyCode.F)){
            CatchKey.Play();
            canvasKey.SetActive(false);
            player.HaveTheKey = true;
            gameObject.SetActive(false);
            Destroy(gameObject,2f);
        }
    }

    private void OnTriggerExit(Collider other) {
        canvasKey.SetActive(false);
    }

}
