﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKey : MonoBehaviour
{
    public BoxCollider coll;
    public GameObject CanvasDoor;
    public Animator animDoor;
    [SerializeField] PlayerMovement player;
    public AudioSource DoorSound;
    private bool door = false;

    void Start(){
        CanvasDoor.SetActive(false);
    }

     private void OnTriggerStay(Collider other) {
            
            if(other.gameObject.tag ==  "Player" && player.HaveTheKey == true){
                CanvasDoor.SetActive(true);
            
            if(Input.GetKeyDown(KeyCode.F)){
                door = true;
                CanvasDoor.SetActive(false);
                animDoor.SetTrigger("open");
                DoorSound.Play();
            }else if(door == true){
                CanvasDoor.SetActive(false);
            }else if( door == false){
                CanvasDoor.SetActive(true);
        }
            

        }
    }
    private void OnTriggerExit(Collider other) {
        
        CanvasDoor.SetActive(false);
    }
}
