﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]

public class DoorOpen : MonoBehaviour
{
    
    public BoxCollider coll;
    public Animator animDoor;
    public AudioSource DoorOpenSound;
   

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag ==  "Player"){
            animDoor.SetTrigger("open");
            DoorOpenSound.Play();
        }
    }
}
