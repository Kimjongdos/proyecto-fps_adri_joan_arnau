﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoManager : MonoBehaviour
{
    void Start(){
        StartCoroutine(PassScene());
    }
    void Update(){
        if(Input.anyKey){
            SceneManager.LoadScene("MenuScreen");
        }
    }

    IEnumerator PassScene(){
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("MenuScreen");
    }
}
