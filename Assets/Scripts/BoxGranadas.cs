﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxGranadas : MonoBehaviour
{
    public SphereCollider coll;
    public GameObject CanvasGranadas;
   
    [SerializeField] GranadeManager granada;
    public AudioSource CatchSound;
    private bool CatchGranada = false;

    void Start(){
        CanvasGranadas.SetActive(false);
    }

     private void OnTriggerStay(Collider other) {
        CanvasGranadas.SetActive(true);
        if(granada.Granadas == 0 && Input.GetKeyDown(KeyCode.F)){
            CatchSound.Play();
            granada.Granadas = 3;
            
        }
           
            

        
    }
    private void OnTriggerExit(Collider other) {
        
        CanvasGranadas.SetActive(false);
    }
}