﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
 
public class PauseManager : MonoBehaviour
{
    private bool isPaused = false;
    [SerializeField] GameObject PauseCanvas;
    public LookRotation camera;
    public PlayerMovement player;

    public FPSInputManager input;
   
  
    public void Continue(){
        Cursor.visible = false;
         Time.timeScale = 1.0f;
         PauseCanvas.SetActive(false);
        isPaused = false;
         camera.enabled = true;
        player.Sensibilidad = 3;
        input.enabled = true;
    }
 
    public void Quit(){
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("MenuScreen");
    }
 
 

    void Update()
    {
        if (!isPaused && Input.GetKeyDown(KeyCode.Escape))
        {
            ActivatePause();
        }
    }
 
    void ActivatePause(){
        Cursor.visible = true;
        isPaused = true;
         Cursor.lockState = CursorLockMode.None;
        PauseCanvas.SetActive(true);
      
       camera.enabled = false;
         player.Sensibilidad = 0;
        input.enabled = false;
 
        Time.timeScale = 0;
    }
    

}