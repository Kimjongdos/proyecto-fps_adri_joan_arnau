﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndingManager : MonoBehaviour
{
   
 

   
    void Start(){
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void Restart(){
        SceneManager.LoadScene("GameplayScreen");
    }

    public void Menu(){
        SceneManager.LoadScene("MenuScreen");
    }
    public void Exit(){
        Application.Quit();
    }
}
