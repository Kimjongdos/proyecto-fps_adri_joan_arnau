﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsManager : MonoBehaviour
{
    private AudioSource click;
    void Awake(){
        click = GetComponent<AudioSource>();
    }
    void Update(){
        if(Input.GetKeyDown(KeyCode.Escape)){
            SceneManager.LoadScene("MenuScreen");
        }
    }
    public void ReturnMenu(){
        click.Play();
        SceneManager.LoadScene("MenuScreen");
    }
}
