﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraMenu : MonoBehaviour
{
    [SerializeField] Animator doorAnim;

    private void DoorEvent(){
        doorAnim.SetTrigger("OpenDoor");
    }
}
