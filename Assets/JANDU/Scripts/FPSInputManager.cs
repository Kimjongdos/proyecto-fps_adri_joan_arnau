﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInputManager : MonoBehaviour {

    private PlayerMovement playerController;
    private float sensitivity = 3.0f;
    private LookRotation lookRotation;
    private MouseCursor mouseCursor;
    private Gun gun;
    private BallShoot ballShoot;



    void Start ()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        lookRotation = playerController.GetComponent<LookRotation>();

        
    
        mouseCursor = new MouseCursor();
        mouseCursor.HideCursor();
    }

    void Update ()
    {
        //El movimiento del player
        Vector2 inputAxis = Vector2.zero;
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");
        playerController.SetAxis(inputAxis);
        //El salto del player
        if(Input.GetButton("Jump")) playerController.StartJump();

        
        
       

        //Cursor del ratón
        if(Input.GetMouseButtonDown(0)) mouseCursor.HideCursor();
        else if(Input.GetKeyDown(KeyCode.Escape)) mouseCursor.ShowCursor();

        
    
      
      

    }
}
