﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookRotation : MonoBehaviour {

    public int Sensibilidad;
    

    
    public float minAngle = -60;
    public float maxAngle = 60;

    private float MouseX = 0;
    private float MouseY = 0;


     // Update is called once per frame
    void Update ()
    {
        MouseX -= Input.GetAxis("Mouse Y") * Sensibilidad;
        MouseX = Mathf.Clamp(MouseX,minAngle,maxAngle);
        transform.localEulerAngles = new Vector3 (MouseX,0,0);
       

    }

  
   
}
